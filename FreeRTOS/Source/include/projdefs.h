/*
    FreeRTOS V7.5.3 - Copyright (C) 2013 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that has become a de facto standard.             *
     *                                                                       *
     *    Help yourself get started quickly and support the FreeRTOS         *
     *    project by purchasing a FreeRTOS tutorial book, reference          *
     *    manual, or both from: http://www.FreeRTOS.org/Documentation        *
     *                                                                       *
     *    Thank you!                                                         *
     *                                                                       *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    >>! NOTE: The modification to the GPL is included to allow you to distribute
    >>! a combined work that includes FreeRTOS without being obliged to provide
    >>! the source code for proprietary components outside of the FreeRTOS
    >>! kernel.

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available from the following
    link: http://www.freertos.org/a00114.html

    1 tab == 4 spaces!

    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?"                                     *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org - Documentation, books, training, latest versions,
    license and Real Time Engineers Ltd. contact details.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.OpenRTOS.com - Real Time Engineers ltd license FreeRTOS to High
    Integrity Systems to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

#ifndef PROJDEFS_H
#define PROJDEFS_H

#include <xc.h>
#include <GenericTypeDefs.h>
/* Kernel includes. */

/* RFLINK start */
#include "peripheral/dma.h"
#include "../../../mavlink/v1.0/common/mavlink.h"
/* RFLINK stop */

/* Defines the prototype to which task functions must conform. */
typedef void (*pdTASK_CODE)( void * );

#define pdFALSE		( ( portBASE_TYPE ) 0 )
#define pdTRUE		( ( portBASE_TYPE ) 1 )

#define pdPASS									( pdTRUE )
#define pdFAIL									( pdFALSE )
#define errQUEUE_EMPTY							( ( portBASE_TYPE ) 0 )
#define errQUEUE_FULL							( ( portBASE_TYPE ) 0 )

/* Error definitions. */
#define errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY	( -1 )
#define errNO_TASK_TO_RUN						( -2 )
#define errQUEUE_BLOCKED						( -4 )
#define errQUEUE_YIELD							( -5 )

#define SYS_FREQ        (80000000L)

#define	GetSystemClock() 	(80000000ul)
#define	GetPeripheralClock()	(GetSystemClock()/(1 << OSCCONbits.PBDIV))
#define	GetInstructionClock()	(GetSystemClock())

/* ---------------------------- */

#ifndef MAVLINK_UART
#define MAVLINK_UART UART1
#endif

#define MAVLINK_BAUD 57600

#define MAVLINK_UART_VECTOR INT_UART_1_VECTOR
#define MAVLINK_INT_RX      INT_U1RX
#define MAVLINK_INT_TX      INT_U1TX

/* -------------------------------- */
/* Defines for some constants start */

/* Define for Pi */
#define PROJDEFS_CONST_PI 3.14159265

/* Define RAD to Degree */
#define PROJDEFS_CONST_RAD_2_DEG 57.295779513082320876798154814105

/* Define Degree to RAD */
#define PROJDEFS_CONST_DEG_2_RAD 0.01745329251994

/* Defines for some constants stop  */
/* -------------------------------- */
/* Definitions for LEDs start       */

#define LED_LIVE 1 << 3

#define LED_SET_ODC      ODCASET =  LED_LIVE
#define LED_SET_OUTPUT   TRISACLR = LED_LIVE
#define LED_SET_DEFAULT  LATACLR =  LED_LIVE

#define LED_LIVE_ON  LATASET = LED_LIVE
#define LED_LIVE_OFF LATACLR = LED_LIVE
#define LED_LIVE_TOG LATAINV = LED_LIVE

/* Definitions for LEDs stop  */
/* -------------------------- */

/* RFLINK start */
#define PROJDEFS_CONF_MCU_FREE          5000

#define PROJDEFS_CONF_MAVLINK_MSG_SIZE   MAVLINK_MAX_PACKET_LEN
#define PROJDEFS_CONF_MAVLINK_MSG_DEEP   8

typedef struct
{
    char msg[PROJDEFS_CONF_MAVLINK_MSG_SIZE];
    unsigned short len;
} PROJDEFS_CONF_MAVLINK_MSG;

typedef union 
{
    unsigned int all_data;

    struct {
        unsigned int enable:1;
        unsigned int tx_busy:1;
    };
} PROJDEFS_CONF_UART_STATE;

/* State of RX FSM RFLINK UART */
typedef enum
{
    MAVLINK_RX_WAIT        = 0,
    MAVLINK_RX_START_FOUND = 1,
    MAVLINK_RX_SEQ         = 2,
    MAVLINK_RX_SYSID       = 3,
    MAVLINK_RX_COMPID      = 4,
    MAVLINK_RX_MSG_TYPE    = 5,
    MAVLINK_RX_PAYLOAD_RCV = 6,
    MAVLINK_RX_CRC_1       = 7,
    MAVLINK_RX_CRC_2       = 8,
    MAVLINK_RX_ERROR       = 9,
} PROJDEFS_MAVLINK_RX_STATE;

/* Define time for understanding witch link with ground lost */
/* unit is seconds, maximum value is 31 seconds */
#define PROJDEFS_MAVLINK_TIMEOUT  10

typedef union
{
    unsigned int all_data;

    struct {
        unsigned int enable:1;
        unsigned int lost_ground:1;
        unsigned int link_state:1;
        unsigned int link_prev_state:1;
        unsigned int force_data_send:1;
        unsigned int timeout_cnt:5;
        unsigned int msg_fsm:8;
        unsigned int ground_station_id:8;
    };
} PROJDEFS_CONF_MAVLINK;

/* How many MAVLINK messages send at the same time */
#define PROJDEFS_MAVLINK_MSG_CNT  4
/* MAVLINK shedule table size */
#define PROJDEFS_MAVLINK_SCHED_SIZE 8

/* Datastructure for MAVLINK message sheduler */
typedef struct
{
    /* ID of subsystem */
    unsigned char compid;
    /* message type */
    unsigned char msg_num;
    /* message period, milliseconds */
    unsigned int  msg_period;
    /* send message after this time */
    unsigned int  start_time;
} PROJDEFS_MAVLINK_SHED_STRUCT;
/* RFLINK start */

/* Main data structure of autopilot start */

typedef struct
{
    /* RFLINK start */
    unsigned short                 mcu_load;
    PROJDEFS_CONF_UART_STATE        mavuart;
    PROJDEFS_MAVLINK_RX_STATE    mavlink_rx;
    /* Mavlink protocol data */
    mavlink_system_t         mavlink_system;
    unsigned char           mav_system_type;
    unsigned char          mav_system_state;
    unsigned char        mav_autopilot_type;
    unsigned char           mav_system_mode;
    unsigned int            mav_custom_mode;
    unsigned short      mav_packet_rx_drops;
    unsigned short         mav_packet_rx_ok;
    PROJDEFS_CONF_MAVLINK           mav_msg;
    MAV_SYS_STATUS_SENSOR   sensors_present;
    MAV_SYS_STATUS_SENSOR   sensors_enabled;
    MAV_SYS_STATUS_SENSOR    sensors_health;
    /* RFLINK stop */
} PROJDEFS_PIPILOT;

/* Main data structure of autopilot stop */

/* Defines for IDLE resource counter start */

typedef struct
{
    unsigned long idle_cnt;
    unsigned int idle_period_cnt;
    unsigned char show_fl:1;
    unsigned long idle_value;
} PROJDEFS_IDLE_STAT;

/* Defines for IDLE resource counter stop  */

/* Init generic GPIOs */
void vProjdefsInitGPIO(void);

/* compute the 2's compliment of int value val */
short sProjdefs2Compire(short val, unsigned char bits);

#endif /* PROJDEFS_H */
