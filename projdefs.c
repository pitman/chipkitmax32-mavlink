/* 
 *
 * PIC32MX with FreeRTOS and MAVLINK integration project
 *
 * Some functions
 * 
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "projdefs.h"
#include <stdio.h>
#include <stdlib.h>
#include <p32xxxx.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

#include "peripheral/uart.h"

/* Init generic GPIOs */
void vProjdefsInitGPIO(void)
{
    /* Disable JTAG port, usefull for some GPIO function */
    DDPCONbits.JTAGEN = 0;

    /* Configure LEDs */
    LED_SET_OUTPUT;
    LED_SET_DEFAULT;
}

/* compute the 2's compliment of int value val */
short sProjdefs2Compire(short val, unsigned char bits)
{
    if ((val & (1 << (bits-1))) != 0 )
    {
        val = val - (1<<bits);
    }
    return val;
}

/* MAVLINK start */
/* UART configure */
void vProjdefsConfigureUART(void)
{
    UARTConfigure(MAVLINK_UART, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetLineControl(MAVLINK_UART, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetFifoMode(MAVLINK_UART, UART_INTERRUPT_ON_TX_BUFFER_EMPTY | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetDataRate(MAVLINK_UART, GetPeripheralClock(), MAVLINK_BAUD);
    UARTEnable(MAVLINK_UART, UART_ENABLE_FLAGS(UART_ENABLE | UART_PERIPHERAL | UART_RX | UART_TX));
    /* Configure UART Interrupt */
    INTDisableInterrupts();
    INTSetVectorPriority(MAVLINK_UART_VECTOR, INT_PRIORITY_LEVEL_2);
    INTSetVectorSubPriority(MAVLINK_UART_VECTOR, INT_SUB_PRIORITY_LEVEL_3);
    INTEnable(MAVLINK_INT_RX, INT_ENABLED);
    INTEnableInterrupts();
}
/* MAVLINK stop */
