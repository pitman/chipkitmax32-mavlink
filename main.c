/* 
 * File:   main.c
 *
 * PIC32MX with FreeRTOS and MAVLINK integration project
  *
 * Main file
 *
 * Copyright (C) Dmitry V. Belimov 2014
 * Email: d.belimov@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <p32xxxx.h>
#include <xc.h>
#include <sys/attribs.h>
#include <math.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "croutine.h"
#include "timers.h"
#include "queue.h"

/* Hardware specific includes. */
#include "ConfigPerformance.h"
#include "peripheral/uart.h"

/* Project setting includes */
#include "projdefs.h"
#include "mavlink/v1.0/common/mavlink.h"

/* Core configuratin fuse settings */

#pragma config FPLLIDIV = DIV_2 /* External crystal is 8MHz, divide it by 2 */
#pragma config FPLLMUL = MUL_20 /* Set Freq multipler to 20, core Freq 80MHz */
#pragma config FPLLODIV = DIV_1 /* Didn't divide core Freq after multiplier */
#pragma config FWDTEN = OFF     /* Disable Watchdog */
#pragma config POSCMOD = HS     /* External high-speed crystal select */
#pragma config FNOSC = PRIPLL   /* Enable PLL for Freq from HS crystal */
#pragma config FPBDIV = DIV_2   /* Set divider for peripherial bus clock to 2 */
#pragma config FSOSCEN = OFF    /* Secondary Oscillator Disable */
#pragma config UPLLEN = ON      /* USB PLL ON */
#pragma config UPLLIDIV = DIV_2 /* Divide 8MHz input to 4Mhz befor USB PLL */
#pragma config OSCIOFNC = OFF   /* CLKO output signal Disable */
#pragma config FCKSM = CSDCMD   /* Clock Switching & Fail Safe Clock Monitor */
#pragma config IESO = ON        /* Internal/External Switch-over */

#pragma config FCANIO = OFF     /* Standard/alternate CAN pin select (OFF=Alt) */
#pragma config FMIIEN = OFF     /* Standard/alternate ETH pin select (OFF=Alt) */
#pragma config FETHIO = OFF     /* MII/RMII select (OFF=RMII) */

#pragma config CP = OFF         /* Code protection OFF */
#pragma config BWP = OFF        /* Boot flash write protection OFF */
#pragma config PWP = OFF        /* Programm flash write protection OFF */

#pragma config FSRSSEL = PRIORITY_7 /* SRS interrupt priority */

#pragma config ICESEL = ICS_PGx2 /* ICE EMUC1/EMUD1 pins shared with PGC1/PGD1 */
#pragma config DEBUG = OFF       /* Background debug */

/*
 * Set up the hardware ready to run this demo.
 */
static void prvSetupHardware( void );

/* MAVLINK start */
/* Queue for TX data to ground */
volatile xQueueHandle xQueueMAVLINKTX;
/* Queue for RX data from a ground */
volatile xQueueHandle xQueueMAVLINKRX;
/* MAVLINK stop */

/* Local time with 1ms resolution */
volatile unsigned int ulLOCALTIME = 0;

/* IDLE free resource counter */
volatile PROJDEFS_IDLE_STAT stats;

/* Main data structure of Pi-Pilot */
volatile PROJDEFS_PIPILOT pilot;

/* Main task for LEDs */
void prvLEDSd(void *pvParameters)
{
    for(;;)
    {
        LED_LIVE_TOG;

        vTaskDelay(500/portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}

/* MAVLINK start */
/* TX service task */
void prvUARTLINKTXd(void *pvParameters)
{
    static PROJDEFS_CONF_MAVLINK_MSG msg;

    for(;;)
    {
        if (pilot.mavuart.enable)
        {
            if (!pilot.mavuart.tx_busy)
            {
                if (uxQueueMessagesWaiting(xQueueMAVLINKTX) != 0)
                {
                    INTEnable(MAVLINK_INT_TX, INT_ENABLED);
                }
            }
            vTaskDelay(1/portTICK_RATE_MS);
        }
        else
        {
            vTaskDelay(200/portTICK_RATE_MS);
        }
    }
    vTaskDelete(NULL);
}

void __attribute__( (interrupt(ipl0), vector(_UART_1_VECTOR), nomips16)) vMAVLINKInterruptWrapper( void );

/* Handler RFLINK RX interrupt */
void vMAVLINKInterruptHandler(void)
{
    static portBASE_TYPE xHigherPriorityTaskWoken;
    static PROJDEFS_CONF_MAVLINK_MSG msgmav_rx, msgmav_tx;
    static unsigned short k, tx_byte_cnt;
    static BYTE rx_byte;

    /* We want woken a task at the start of the ISR. */
    xHigherPriorityTaskWoken = pdFALSE;

    /* Send MAVLINK data packet */
    if (INTGetFlag(MAVLINK_INT_TX) && INTGetEnable(MAVLINK_INT_TX)) /* transmit buffer empty? */
    {
        if (!pilot.mavuart.tx_busy)
        {
            if (uxQueueMessagesWaitingFromISR(xQueueMAVLINKTX) != 0)
            {
                /* Get MAVLINK message from Queue */
                xQueueReceiveFromISR(xQueueMAVLINKTX, &msgmav_tx, &xHigherPriorityTaskWoken);
                pilot.mavuart.tx_busy = 1;
                /* init counter */
                tx_byte_cnt = 1;
                /* start send a data */
                UARTSendDataByte(MAVLINK_UART, msgmav_tx.msg[0]);
            }
            else
            {
                /* No more packet for send */
                pilot.mavuart.tx_busy = 0;
                INTEnable(MAVLINK_INT_TX, INT_DISABLED);
            }
        }
        else
        {
            UARTSendDataByte(MAVLINK_UART, msgmav_tx.msg[tx_byte_cnt]);
            tx_byte_cnt++;
            if (tx_byte_cnt >= msgmav_tx.len)
            {
                /* No more byte in a packet for send */
                pilot.mavuart.tx_busy = 0;
                
                /* More packet in Queue for send? */
                if (uxQueueMessagesWaitingFromISR(xQueueMAVLINKTX) == 0)
                    INTEnable(MAVLINK_INT_TX, INT_DISABLED);
            }
        }

        mU1TXClearIntFlag();
    }

    /* Receive MAVLINK data packet */
    if (INTGetFlag(MAVLINK_INT_RX) && INTGetEnable(MAVLINK_INT_RX))
    {
        rx_byte = UARTGetDataByte(MAVLINK_UART);

        switch (pilot.mavlink_rx)
        {
        case MAVLINK_RX_WAIT:
        case MAVLINK_RX_ERROR:
            if (rx_byte == MAVLINK_STX)
            {
                pilot.mavlink_rx = MAVLINK_RX_START_FOUND;
                k = 0;
                msgmav_rx.msg[0] = rx_byte;
            }
            break;
        case MAVLINK_RX_START_FOUND:
            msgmav_rx.len = rx_byte;
            msgmav_rx.msg[1] = rx_byte;
            pilot.mavlink_rx = MAVLINK_RX_SEQ;
            break;
        case MAVLINK_RX_SEQ:
            msgmav_rx.msg[2] = rx_byte;
            pilot.mavlink_rx = MAVLINK_RX_SYSID;
            break;
        case MAVLINK_RX_SYSID:
            msgmav_rx.msg[3] = rx_byte;
            pilot.mavlink_rx = MAVLINK_RX_COMPID;
            break;
        case MAVLINK_RX_COMPID:
            msgmav_rx.msg[4] = rx_byte;
            pilot.mavlink_rx = MAVLINK_RX_MSG_TYPE;
            break;
        case MAVLINK_RX_MSG_TYPE:
            msgmav_rx.msg[5] = rx_byte;
            pilot.mavlink_rx = MAVLINK_RX_PAYLOAD_RCV;
            break;
        case MAVLINK_RX_PAYLOAD_RCV:
            if (k >= (msgmav_rx.len))
            {
                pilot.mavlink_rx = MAVLINK_RX_CRC_1;
            }
            msgmav_rx.msg[k + 6] = rx_byte;
            k++;
            break;
        case MAVLINK_RX_CRC_1:
            msgmav_rx.msg[msgmav_rx.len + 7] = rx_byte;
            pilot.mavlink_rx = MAVLINK_RX_CRC_2;
            break;
        case MAVLINK_RX_CRC_2:
            msgmav_rx.msg[msgmav_rx.len + 8] = rx_byte;
            msgmav_rx.len += 8;
            pilot.mavlink_rx = MAVLINK_RX_WAIT;
            /* Send received mavlinkpacet here */
            xQueueSendFromISR(xQueueMAVLINKRX, &msgmav_rx, &xHigherPriorityTaskWoken);
            break;
        }            

        mU1RXClearIntFlag();
    }

    /* If sending or receiving necessitates a context switch, then switch now. */
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

/* Service task for received mavlink pockets */
void prvUARTLINKRXd(void *pvParameters)
{
    static PROJDEFS_CONF_MAVLINK_MSG msgmav_rx;
    static mavlink_message_t msg;
    static mavlink_status_t status;
    static mavlink_request_data_stream_t request_data_stream;
    static unsigned short k;

    for(;;)
    {
        /* receive new data */
        xQueueReceive(xQueueMAVLINKRX, &msgmav_rx, portMAX_DELAY);

        for (k = 0; k < msgmav_rx.len; k++)
        {
            if(mavlink_parse_char(MAVLINK_COMM_0, msgmav_rx.msg[k], &msg, &status))
            {
                if (msg.sysid == pilot.mav_msg.ground_station_id)
                {
                    switch(msg.msgid)
                    {
                    case MAVLINK_MSG_ID_HEARTBEAT:
                        /* restore link after lost */
                        if (!pilot.mav_msg.link_state)
                        {
                            pilot.mav_msg.link_state = 1;
                            pilot.mav_msg.link_prev_state = 0;
                            /* Link established */
                            /* start some task here */
                        }
                        /* Clear mavlink timeout counter */
                        pilot.mav_msg.timeout_cnt = 0;
                        break;
#if 0
                    case MAVLINK_MSG_ID_REQUEST_DATA_STREAM:
                        mavlink_msg_request_data_stream_decode(&msg, &request_data_stream);
                        switch (request_data_stream.req_stream_id)
                        {

                        }
                        break;
                    case MAVLINK_MSG_ID_COMMAND_LONG:
                        break;
#endif
                        default:
                        //Do nothing
                        break;
		    }
                    break;
                }

                pilot.mav_packet_rx_drops += status.packet_rx_drop_count;
                pilot.mav_packet_rx_ok += status.packet_rx_success_count;
            }
        }
    }
    vTaskDelete(NULL);
}

void prvMAVLINKd(void *pvParameters)
{
    static PROJDEFS_MAVLINK_SHED_STRUCT msg_sched_table[PROJDEFS_MAVLINK_SCHED_SIZE] = {
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_HEARTBEAT, 1000, 0},
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_SYS_STATUS, 5000, 0},
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_SYSTEM_TIME, 5000, 0},
        {MAV_COMP_ID_IMU, MAVLINK_MSG_ID_SCALED_IMU, 40, 0},
        {MAV_COMP_ID_IMU, MAVLINK_MSG_ID_SCALED_PRESSURE, 1000, 0},
        {MAV_COMP_ID_IMU, MAVLINK_MSG_ID_ATTITUDE, 40, 0},
        {MAV_COMP_ID_ALL, MAVLINK_MSG_ID_RC_CHANNELS_SCALED, 40, 0},
        {MAV_COMP_ID_SERVO1, MAVLINK_MSG_ID_SERVO_OUTPUT_RAW, 40, 0},
    };
    static PROJDEFS_CONF_MAVLINK_MSG msgmav;
    static mavlink_message_t msg;
    static unsigned char k, msg_cnt,msg_fl;

    for(;;)
    {
        /* Send MAVLINK messages if enabled */
        if (pilot.mav_msg.enable)
        {
            /* Send messages if link is ready or force mode */
            if ((pilot.mav_msg.link_state) || (pilot.mav_msg.force_data_send))
            {
                msg_cnt = 0;

                for (k = 0; k < PROJDEFS_MAVLINK_SCHED_SIZE; k++)
                {
                    if (ulLOCALTIME > msg_sched_table[k].start_time)
                    {
                        msg_fl = 0;

                        switch (msg_sched_table[k].msg_num)
                        {
                        case MAVLINK_MSG_ID_HEARTBEAT:
                            mavlink_msg_heartbeat_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg,
                                    pilot.mav_system_type, pilot.mav_autopilot_type,
                                    pilot.mav_system_mode, pilot.mav_custom_mode,
                                    pilot.mav_system_state);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SYS_STATUS:
                            mavlink_msg_sys_status_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, pilot.sensors_present,
                                    pilot.sensors_enabled, pilot.sensors_health,
                                    pilot.mcu_load, 11250, 5, 100, 0, 0, 0, 0, 0, 0);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SYSTEM_TIME:
                            mavlink_msg_system_time_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, 0, ulLOCALTIME);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SCALED_IMU:
                            mavlink_msg_scaled_imu_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, ulLOCALTIME,
                                    30, 50, 1000, 20, 30, 60, 3459, 34, 586);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SCALED_PRESSURE:
                            mavlink_msg_scaled_pressure_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, ulLOCALTIME,
                                    65765.34, 0, 2870);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_ATTITUDE:
                            mavlink_msg_attitude_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, ulLOCALTIME,
                                    0.1, 0.2, 30.0, 0.01, 0.03, 0.1);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_RC_CHANNELS_SCALED:
                            mavlink_msg_rc_channels_scaled_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, ulLOCALTIME,
                                    1, 500, 0, -10000, 0, 0, 10000, 10000, 0, 255);
                            msg_fl = 1;
                            break;
                        case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW:
                            mavlink_msg_servo_output_raw_pack(pilot.mavlink_system.sysid,
                                    msg_sched_table[k].compid, &msg, ulLOCALTIME,
                                        1, 1500, 1500, 2200, 900, 1670, 1040,
                                        1970, 1980);
                            msg_fl = 1;
                            break;
                        }

                        /* Set new message time */
                        msg_sched_table[k].start_time = ulLOCALTIME + msg_sched_table[k].msg_period;

                        msg_cnt++;

                        if (msg_fl)
                        {
                            /* Copy the message to the send buffer */
                            msgmav.len = mavlink_msg_to_send_buffer(msgmav.msg, &msg);
                            xQueueSend(xQueueMAVLINKTX, &msgmav, 0);
                        }
                        /* Send few messages and exit, other will be send at the next time */
                        if (msg_cnt >= PROJDEFS_MAVLINK_MSG_CNT)
                            goto fexit;
                    }
                }
            }
fexit:      vTaskDelay(10/portTICK_RATE_MS);
        }
        else
        {
            vTaskDelay(200/portTICK_RATE_MS);
        }
    }
    vTaskDelete(NULL);
}
/* RFLINK stop */

/*
 * 
 */
int main(void) {
    portBASE_TYPE xResult;

    prvSetupHardware();

    /* Register Queues */

    /* MAVLINK start */
    /* Init Queue for transmit data to ground */
    xQueueMAVLINKTX = xQueueCreate(PROJDEFS_CONF_MAVLINK_MSG_DEEP,
            sizeof(PROJDEFS_CONF_MAVLINK_MSG));

    /* Init Queue for receive data from ground */
    xQueueMAVLINKRX = xQueueCreate(PROJDEFS_CONF_MAVLINK_MSG_DEEP,
            sizeof(PROJDEFS_CONF_MAVLINK_MSG));
    /* MAVLINK stop */

    /* Register tasks */
    /* The main task for LEDs */
    xResult = xTaskCreate( prvLEDSd, ( signed char * ) "prvLEDSd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);

    /* MAVLINK start */
    /* Service task for transmit data to ground */
    xResult = xTaskCreate( prvUARTLINKTXd, ( signed char * ) "prvUARTLINKTXd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);

    /* Service task for receive data from ground */
    xResult = xTaskCreate( prvUARTLINKRXd, ( signed char * ) "prvUARTLINKRXd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);

    /* Service task for make and send MAVLINK messages  */
    xResult = xTaskCreate( prvMAVLINKd, ( signed char * ) "prvMAVLINKd",
            configMINIMAL_STACK_SIZE, NULL, 1, NULL);
    /* MAVLINK stop */

    /* Start the scheduler. */
    vTaskStartScheduler();

    for( ;; );

    return (EXIT_SUCCESS);
}

// ****************************************************************************
// ****************************************************************************
// Application Main Entry Point
// ****************************************************************************
// ****************************************************************************

static void prvSetupHardware( void )
{
    static unsigned char k;

    /* Configure the hardware for maximum performance. */
    vHardwareConfigurePerformance();

    /* Setup to use the external interrupt controller. */
    vHardwareUseMultiVectoredInterrupts();

    portDISABLE_INTERRUPTS();

    /* Init some data structure */

    /* MAVLINK start */
    /* Configure RFLINK`s UART */
    pilot.sensors_present = MAV_SYS_STATUS_SENSOR_3D_GYRO | MAV_SYS_STATUS_SENSOR_3D_ACCEL |
            MAV_SYS_STATUS_SENSOR_3D_MAG | MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE |
            MAV_SYS_STATUS_SENSOR_MOTOR_OUTPUTS | MAV_SYS_STATUS_SENSOR_RC_RECEIVER;
    pilot.sensors_enabled = MAV_SYS_STATUS_SENSOR_3D_GYRO | MAV_SYS_STATUS_SENSOR_3D_ACCEL |
            MAV_SYS_STATUS_SENSOR_3D_MAG | MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE |
            MAV_SYS_STATUS_SENSOR_RC_RECEIVER;
    pilot.sensors_health = MAV_SYS_STATUS_SENSOR_3D_GYRO | MAV_SYS_STATUS_SENSOR_3D_ACCEL |
            MAV_SYS_STATUS_SENSOR_ABSOLUTE_PRESSURE |
            MAV_SYS_STATUS_SENSOR_MOTOR_OUTPUTS | MAV_SYS_STATUS_SENSOR_RC_RECEIVER;
    pilot.mavuart.enable = 1;
    pilot.mavuart.tx_busy = 0;
    pilot.mavlink_rx = MAVLINK_RX_WAIT;
    /* Configure Mavlink */
    pilot.mav_system_type = MAV_TYPE_FIXED_WING;
    pilot.mav_autopilot_type = MAV_AUTOPILOT_GENERIC;
    pilot.mav_system_mode = MAV_MODE_PREFLIGHT;
    pilot.mav_custom_mode = 0;
    pilot.mav_system_state = MAV_STATE_STANDBY;
    pilot.mavlink_system.sysid = 20;
    pilot.mavlink_system.type = MAV_TYPE_FIXED_WING;
    pilot.mav_packet_rx_drops = 0;
    pilot.mav_packet_rx_ok = 0;
    pilot.mav_msg.all_data = 0x00000000;
    pilot.mav_msg.enable = 1;
    pilot.mav_msg.force_data_send = 1;
    pilot.mav_msg.ground_station_id = 255;
    /* RFLINK stop */

    /* Init statistics counters */
    stats.idle_cnt = 0;
    stats.idle_period_cnt = 0;

    /* Init generic GPIOs */
    vProjdefsInitGPIO();
    /* Init UART */
    vProjdefsConfigureUART();

//	The USB specifications require that USB peripheral devices must never source
//	current onto the Vbus pin.  Additionally, USB peripherals should not source
//	current on D+ or D- when the host/hub is not actively powering the Vbus line.
//	When designing a self powered (as opposed to bus powered) USB peripheral
//	device, the firmware should make sure not to turn on the USB module and D+
//	or D- pull up resistor unless Vbus is actively powered.  Therefore, the
//	firmware needs some means to detect when Vbus is being powered by the host.
//	A 5V tolerant I/O pin can be connected to Vbus (through a resistor), and
// 	can be used to detect when Vbus is high (host actively powering), or low
//	(host is shut down or otherwise not supplying power).  The USB firmware
// 	can then periodically poll this I/O pin to know when it is okay to turn on
//	the USB module/D+/D- pull up resistor.  When designing a purely bus powered
//	peripheral device, it is not possible to source current on D+ or D- when the
//	host is not actively providing power on Vbus. Therefore, implementing this
//	bus sense feature is optional.  This firmware can be made to use this bus
//	sense feature by making sure "USE_USB_BUS_SENSE_IO" has been defined in the
//	HardwareProfile.h file.
    #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = INPUT_PIN; // See HardwareProfile.h
    #endif

//	If the host PC sends a GetStatus (device) request, the firmware must respond
//	and let the host know if the USB peripheral device is currently bus powered
//	or self powered.  See chapter 9 in the official USB specifications for details
//	regarding this request.  If the peripheral device is capable of being both
//	self and bus powered, it should not return a hard coded value for this request.
//	Instead, firmware should check if it is currently self or bus powered, and
//	respond accordingly.  If the hardware has been configured like demonstrated
//	on the PICDEM FS USB Demo Board, an I/O pin can be polled to determine the
//	currently selected power source.  On the PICDEM FS USB Demo Board, "RA2"
//	is used for	this purpose.  If using this feature, make sure "USE_SELF_POWER_SENSE_IO"
//	has been defined in HardwareProfile - (platform).h, and that an appropriate I/O pin
//  has been mapped	to it.
    #if defined(USE_SELF_POWER_SENSE_IO)
    tris_self_power = INPUT_PIN;	// See HardwareProfile.h
    #endif

}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue,
	timer or semaphore is created.  It is also called by various parts of the
	demo application.  If heap_1.c or heap_2.c are used, then the size of the
	heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	to query the size of free heap space that remains (although it does not
	provide information on how the remaining heap might be fragmented). */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
	to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
	task.  It is essential that code added to this hook function never attempts
	to block in any way (for example, call xQueueReceive() with a block time
	specified, or call vTaskDelay()).  If the application makes use of the
	vTaskDelete() API function (as this demo application does) then it is also
	important that vApplicationIdleHook() is permitted to return to its calling
	function, because it is the responsibility of the idle task to clean up
	memory allocated by the kernel to any task that has since been deleted. */


    /* show CPU free IDLE statistics */
    if (stats.show_fl)
    {
        stats.show_fl = 0;
    }

    /* CPU free resourse counter */
    stats.idle_cnt++;
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time task stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook	function is
	called if a task stack overflow is detected.  Note the system/interrupt
	stack is not checked. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
    /* This function will be called by each tick interrupt if
       configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
       added here, but the tick hook is called from an interrupt context, so
       code must not attempt to block, and only the interrupt safe FreeRTOS API
       functions can be used (those that end in FromISR()). */

    ulLOCALTIME++;

    /* CPU free resourse counter some code */
    stats.idle_period_cnt++;
    /* show idle stat every 1 sec. */
    if (stats.idle_period_cnt >= 1000)
    {
        stats.idle_value = stats.idle_cnt;
        stats.show_fl = 1;
        stats.idle_period_cnt = 0;
        stats.idle_cnt = 0;
        /* RFLINK start */
        pilot.mcu_load = (unsigned short)(1000 - stats.idle_value / PROJDEFS_CONF_MCU_FREE);
        /* Mavlink */
        if (pilot.mav_msg.link_state)
        {
            pilot.mav_msg.timeout_cnt++;
            if (pilot.mav_msg.timeout_cnt > PROJDEFS_MAVLINK_TIMEOUT)
            {
                pilot.mav_msg.link_state = 0;
                pilot.mav_msg.link_prev_state = 1;
                /* No link with ground station */
                /* do somethink here */
                
            }
        }
        /* RFLINK stop */
    }
}
/*-----------------------------------------------------------*/

void _general_exception_handler( unsigned long ulCause, unsigned long ulStatus )
{
    /* This overrides the definition provided by the kernel.  Other exceptions
    should be handled here. */

    LED_LIVE_OFF;
    for( ;; );
}
